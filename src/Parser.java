/*
 * Copyright LittleCube 2022
 */

import java.io.File;
import java.util.Scanner;

public class Parser
{
	private String codeString;
	private int indexCounter;
	
	private LinkedList<String> delimiters;
	
	public Parser()
	{
		codeString = "";
		indexCounter = 0;
		
		delimiters = new LinkedList<String>();
	}
	
	public Parser(String newCodeString, boolean ... isPath)
	{
		this();
		
		if (isPath.length == 0 || !isPath[0])
		{
			setCodeString(newCodeString);
		}
		
		else if (isPath[0])
		{
			try
			{
				File smpFile = new File(newCodeString);
				
				if (!smpFile.exists())
				{
					throw new Exception("File not found: " + newCodeString);
				}
				
				Scanner s = new Scanner(smpFile);
				
				while (s.hasNextLine())
				{
					codeString += s.nextLine() + "\n";
				}
				
				codeString = codeString.substring(0, codeString.length() - 1);
				
				s.close();
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void setCodeString(String newCodeString)
	{
		codeString = newCodeString;
	}
	
	public void addDelimiter(String delimiter)
	{
		delimiters.add(delimiter);
	}
	
	public void setCounter(int newCounter)
	{
		indexCounter = newCounter;
	}
	
	public void advanceCounter(int offset)
	{
		indexCounter += offset;
	}
	
	public String nextToken()
	{
		String token = "";
		boolean done = false;
		
		nextDelimiter();
		
		while (indexCounter < codeString.length() && !done)
		{
			while (delimiters.more())
			{
				if (codeString.substring(indexCounter).startsWith(delimiters.current()))
				{
					done = true;
				}
				
				delimiters.next();
			}
			
			if (!done)
			{
				token += codeString.charAt(indexCounter);
				++indexCounter;
			}
			
			delimiters.reset();
		}
		
		return token;
	}
	
	// TODO only allow multiple whitespace characters, and not multiple
	//      of any others
	public String nextDelimiter()
	{
		String delimiter = "";
		boolean stillDelimiter = true;
		
		while (indexCounter < codeString.length() && stillDelimiter)
		{
			stillDelimiter = false;
			
			while (delimiters.more())
			{
				if (codeString.substring(indexCounter).startsWith(delimiters.current()))
				{
					stillDelimiter = true;
				}
				
				delimiters.next();
			}
			
			if (stillDelimiter)
			{
				delimiter += codeString.charAt(indexCounter);
				++indexCounter;
			}
			
			delimiters.reset();
		}
		
		return delimiter;
	}
}