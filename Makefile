####################################################
#                                                  #
#                 everything sucks                 #
#                                                  #
####################################################

CLASSPATH		:= lib
CLASSPATHS		:= -cp $(CLASSPATH)

SOURCES			:= src
BUILD			:= build
BUILDS			:= -d $(BUILD)

JAVAC			:= javac
JAVACFLAGS		:= $(BUILDS) $(CLASSPATHS)



.PHONY: all dev test clean



all: dev

dev: | $(BUILD)
	@echo "compiling..."
	
	@$(JAVAC) $(JAVACFLAGS) src/*.java test/*.java
	@cp -f lib/* build/

test: dev
	@echo "testing..."
	@cd test && java -cp ../$(BUILD) Test

$(BUILD):
	@mkdir $@

clean:
	@echo clean ...
	
	@rm -rf $(BUILD)
