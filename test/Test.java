public class Test
{
	public static void main(String[] args)
	{
		Parser p = new Parser("  bruh  bruh ");
		
		p.addDelimiter(" ");
		
		System.out.println("test 1");
		System.out.println(p.nextToken());
		System.out.println("test 2");
		System.out.println(p.nextToken());
		System.out.println("test 3");
		System.out.println(p.nextToken());
		
		System.out.println("file test");
		
		Parser fileParser = new Parser("Test.smp", true);
		
		fileParser.addDelimiter(" ");
		fileParser.addDelimiter(",");
		fileParser.addDelimiter(";");
		fileParser.addDelimiter("\n");
		
		System.out.println("test 1");
		System.out.println(fileParser.nextToken());
		System.out.println("test 2");
		System.out.println(fileParser.nextToken());
		System.out.println("test 3");
		System.out.println(fileParser.nextToken());
		System.out.println("test 4");
		System.out.println(fileParser.nextToken());
		System.out.println("test 5");
		System.out.println(fileParser.nextToken());
		System.out.println("test 6");
		System.out.println(fileParser.nextToken());
		System.out.println("test 7");
		System.out.println(fileParser.nextToken());
		System.out.println("test 8");
		System.out.println(fileParser.nextToken());
		System.out.println("test 9");
		System.out.println(fileParser.nextToken());
	}
}